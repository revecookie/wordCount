import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MainTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams()
    {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams()
    {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void filesHaveThreeDifferentWordsEach() throws FileNotFoundException, InterruptedException
    {
        List<String> fileNames = new ArrayList<>(Arrays.asList("first.txt", "second.txt"));
        String expectedOutput = "wordBC 6 = 0 + 6 \nwordBB 5 = 0 + 5 \nwordBA 4 = 0 + 4 \nwordAC 3 = 3 + 0 \nwordAB 2 = 2 + 0 \nwordAA 1 = 1 + 0";

        Main.setFileNames(fileNames);
        Main.main(null);
        assertEquals(expectedOutput, outContent.toString().trim());
    }

    @Test
    public void filesHaveSameWords() throws FileNotFoundException, InterruptedException
    {
        List<String> fileNames = new ArrayList<>(Arrays.asList("sameA.txt", "sameB.txt"));
        String expectedOutput = "hello 10 = 7 + 3 \nis 9 = 5 + 4 \nit 8 = 3 + 5 \nme 7 = 0 + 7 \nyou 6 = 5 + 1 \nare 5 = 3 + 2 \nlooking 4 = 1 + 3 \nfor 3 = 2 + 1";

        Main.setFileNames(fileNames);
        Main.main(null);
        assertEquals(expectedOutput, outContent.toString().trim());
    }

    @Test
    public void oneFileEmpty() throws FileNotFoundException, InterruptedException
    {
        List<String> fileNames = new ArrayList<>(Arrays.asList("first.txt", "empty.txt"));
        String expectedOutput = "wordAC 3 = 3 + 0 \nwordAB 2 = 2 + 0 \nwordAA 1 = 1 + 0";

        Main.setFileNames(fileNames);
        Main.main(null);
        assertEquals(expectedOutput, outContent.toString().trim());
    }

    @Test
    public void bothFilesEmpty() throws FileNotFoundException, InterruptedException
    {
        List<String> fileNames = new ArrayList<>(Arrays.asList("empty.txt", "empty.txt"));

        Main.setFileNames(fileNames);
        Main.main(null);
        assertEquals("", outContent.toString().trim());
    }

    @Test
    public void oneFileMissing() throws FileNotFoundException, InterruptedException
    {
        List<String> fileNames = new ArrayList<>(Arrays.asList("boom!", "first.txt"));

        Main.setFileNames(fileNames);
        Main.main(null);
        assertEquals("wordAC 3 = 0 + 3 \nwordAB 2 = 0 + 2 \nwordAA 1 = 0 + 1", outContent.toString().trim());
        assertEquals("File: boom! could not be found", errContent.toString().trim());
    }

    @Test
    public void twoFilesMissing() throws FileNotFoundException, InterruptedException
    {
        List<String> fileNames = new ArrayList<>(Arrays.asList("boom!", "kaboom!"));

        Main.setFileNames(fileNames);
        Main.main(null);
        assertEquals("", outContent.toString().trim());
        assertEquals("File: boom! could not be found\n" +
                "File: kaboom! could not be found", errContent.toString().trim());
    }
}
