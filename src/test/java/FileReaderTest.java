import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class FileReaderTest {

    @After
    public void cleanUp() {
        FileReader.setSharedCache(new HashMap<>());
    }

    @Test(expected = FileNotFoundException.class)
    public void tryToReadNonExistingFile() throws FileNotFoundException
    {
        FileReader fr = new FileReader("Boom!");
        fr.readFile();
    }

    @Test
    public void readAFileWithNoErrors() throws FileNotFoundException
    {
        String filename = "first.txt";
        FileReader fr = new FileReader(filename);
        fr.readFile();
        Map<String, List<String>> cache = FileReader.getSharedCache();
        Assert.assertTrue(cache.containsKey(filename));
        List<String> words = cache.get(filename);
        Assert.assertEquals(6, words.size());
        Assert.assertTrue(words.contains("wordAA"));
        Assert.assertTrue(words.contains("wordAB"));
        Assert.assertTrue(words.contains("wordAC"));
    }

    @Test
    public void readEmptyFile() throws FileNotFoundException
    {
        String filename = "empty.txt";
        FileReader fr = new FileReader(filename);
        fr.readFile();
        Map<String, List<String>> cache = FileReader.getSharedCache();
        Assert.assertTrue(cache.containsKey(filename));
        List<String> words = cache.get(filename);
        Assert.assertEquals(0, words.size());
    }

    @Test
    public void tryToReadNonTxtFile() throws FileNotFoundException
    {
        String filename = "javascript.js";
        FileReader fr = new FileReader(filename);
        fr.readFile();
        Map<String, List<String>> cache = FileReader.getSharedCache();
        Assert.assertTrue(cache.containsKey(filename));
        List<String> words = cache.get(filename);
        Assert.assertEquals(2, words.size());
        Assert.assertTrue(words.contains("function"));
        Assert.assertTrue(words.contains("something"));
    }

    @Test
    public void cleanUpRemovesNonAlphabeticalCharacters()
    {
        String dirty = "it.";
        Optional<String> clean = FileReader.cleanUpWord(dirty);
        Assert.assertEquals("it", clean.get());
        String dirtyB = ".";
        clean = FileReader.cleanUpWord(dirtyB);
        Assert.assertFalse(clean.isPresent());
    }

}
