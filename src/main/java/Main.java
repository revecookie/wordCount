import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    private static List<String> fileNames = new ArrayList<>(Arrays.asList("first.txt", "second.txt"));


    public static void main(String[] args) throws InterruptedException
    {
        readFiles();
        Map<String, List<String>> cache = FileReader.getSharedCache();
        countWordsAndPrintToConsole(cache);
        //Cleanup cache
        FileReader.setSharedCache(new HashMap<>());

    }

    private static void readFiles() throws InterruptedException
    {
        ExecutorService es = Executors.newCachedThreadPool();
        for (String filename : fileNames)
            es.execute(() -> {
                FileReader fr = new FileReader(filename);
                try {
                    fr.readFile();
                } catch (FileNotFoundException e) {
                    System.err.println("File: " + filename + " could not be found");
                }
            });
        es.shutdown();
        es.awaitTermination(1, TimeUnit.MINUTES);
    }

    private static void countWordsAndPrintToConsole(Map<String, List<String>> cache)
    {
        Runnable processor = () -> {
            Map<String, Counter> occurrencesPerWord = new HashMap<>();

            cache.forEach((filename, wordList) ->

                    wordList.forEach(word -> {
                        if (!occurrencesPerWord.containsKey(word)) {
                            occurrencesPerWord.put(word, new Counter());
                        }

                        Counter c = occurrencesPerWord.get(word);
                        c.incrementFileNameOccurrences(filename);
                        c.incrementTotal();
                    })

            );

            printOccurrencesPerWord(occurrencesPerWord);
        };

        processor.run();
    }

    private static void printOccurrencesPerWord(Map<String, Counter> occurrencesPerWord)
    {
        occurrencesPerWord
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Counter::compare))
                .forEach(e -> System.out.println(e.getKey() + e.getValue().toString(fileNames)));
    }

    public static void setFileNames(List<String> fileNames)
    {
        Main.fileNames = fileNames;
    }
}
