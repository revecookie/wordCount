import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Counter {
    private Integer totalOccurrences = 0;
    private Map<String, Integer> occurrencesPerFile = new HashMap<>(2);

    void incrementTotal() {
        totalOccurrences = totalOccurrences + 1;
    }

    void incrementFileNameOccurrences(String fileName) {
        if(!occurrencesPerFile.containsKey(fileName)) {
            occurrencesPerFile.put(fileName, 0);
        }
        Integer count = occurrencesPerFile.get(fileName);
        occurrencesPerFile.put(fileName, count + 1);
    }

    Integer compare(Counter other) {
        if(this.totalOccurrences > other.totalOccurrences) {
            return -1;
        }
        else if (this.totalOccurrences < other.totalOccurrences) {
            return 1;
        }

        return 0;
    }

    public String toString(List<String> fileNames)
    {
        String perFile = fileNames.stream().map(f -> {
            if(occurrencesPerFile.containsKey(f)) {
                return occurrencesPerFile.get(f) + " + ";
            }
            return 0 + " + ";
        }).collect(Collectors.joining());

        perFile = perFile.substring(0, perFile.length() - 2);
        return " " + totalOccurrences + " = " + perFile;
    }
}
