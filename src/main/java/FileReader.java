import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

class FileReader {

    private String fileName;
    private static Map<String, List<String>> sharedCache = new ConcurrentHashMap<>();

    FileReader(String fileName)
    {
        this.fileName = fileName;
    }

    static Map<String, List<String>> getSharedCache()
    {
        return sharedCache;
    }

    public static void setSharedCache(Map<String, List<String>> sharedCache)
    {
        FileReader.sharedCache = sharedCache;
    }

    void readFile() throws FileNotFoundException
    {
        File file = openFile();
        List<String> fileWords = scanWords(file);
        sharedCache.put(fileName, fileWords);
    }

    private File openFile() throws FileNotFoundException
    {
        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new FileNotFoundException(fileName + " not found");
        }

        return new File(resource.getFile());
    }

    private static List<String> scanWords(File file) throws FileNotFoundException
    {
        List<String> fileWords = new ArrayList<>();
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] splitLine = line.split("\\s+");
            for (String word : splitLine) {
                cleanUpWord(word).ifPresent(fileWords::add);
            }
        }

        scanner.close();
        return fileWords;
    }

    // Eliminates special characters like comma, question mark e.t.c.
    // Empty case a special character (. ? ,) was alone as a separate word. (eg. Hello , World)
    static Optional<String> cleanUpWord(String word)
    {
        String cleanWord = word.replaceAll("[^a-zA-Z]+", "");
        if ("".equals(cleanWord)) {
            return Optional.empty();
        }
        return Optional.of(cleanWord);
    }


}
