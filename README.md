# WORD COUNTER (An assessment for Improve Digital)

This is a simple multithreaded application that reads two files and prints the occurrences of each word, both total and per file.

## How to run
### Prerequisites
1. Java 8 is installed on your machine. 
2. Maven is installed also

### What to do
1. Download the application to your machine. 
2. open a terminal and cd to the project directory
3. Run `mvn package`
4. Run `java -jar target/wordCount-0.0.0.jar`

## Running the tests
1. `mvn clean`
2. `mvn test`

## Assumptions
1. File size does not matter on how we handle the process. Even if the file id millions of words we will still read and transform in the same way
2. We don't care about the size difference in files. Each thread is dedicated to one file.
3. Files contain proper words consisting only of alphanumeric characters ([a-z,A-Z]*) and we ignore special characters
4. If 2 words have the same total count they are printed in random order
5. The order of files is the one given in the initial list (the implementation is generic, we could have as many files as we want)
6. If one file does not exist we just print zeroes for the words and also print a message in the error output
7. If both files are missing then we just print the error messages

If you have any questions please feel free to contact me!